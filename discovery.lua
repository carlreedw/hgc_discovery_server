
DiscoveryServer = {}

local function log(text)
	print(text)
end
log("Initializing Hero Game Co. Discovery Server")
local lobbies = {}
local authentication = {}
-- local peer_uuids = {}
local peers = {}
local last_peer_check = love.timer.getTime()
local max_peers, bandwidth = 1000, 1e6
local verbose_logging = true
local game_list = {
	MASTERSQUEAK = true
}
log ("Creating enet host object")
local host = enet.host_create(server_address or "localhost:6789", max_peers, channel_count, bandwidth, bandwidth)

local function checkPeers()
	-- check each peer
	-- if disconnected, close associated lobbies if applicable and destroy peer obj
	
	local delete_these_peers = {}
	local peer_count = 0
	for connect_id, peer_container in pairs(peers) do
		peer_count = peer_count + 1
		if verbose_logging then
			log('checking peer ' .. tostring(connect_id))
		end
		local peer = peer_container.peer
		local user_uuid = peer_container.user_uuid
		local lobby_id = authentication[user_uuid]
		local state = peer:state()
		
		if state == 'disconnected' then
			if verbose_logging then
				log(tostring(connect_id) .. ' is disconnected')
			end
			local lobby = lobbies[lobby_id]
			if lobby then
				if verbose_logging then
					log('found lobby associated with ' .. tostring(connect_id))
				end
				lobbies[lobby_id] = nil
				authentication[user_uuid] = nil
			end
			table.insert(delete_these_peers, connect_id)
		end
	end
	
	if verbose_logging then
		log(string.format('checked %s peers', peer_count))
	end
	
	for i, id in ipairs(delete_these_peers) do
		if verbose_logging then
			log("setting peers[" .. id .. "] to nil")
		end
		peers[id] = nil
	end
	
	last_peer_check = love.timer.getTime()
end
local function parseRequest(event)
	-- all events are deserialized here first, then routed to the appropriate function if possible
	local ok, request = serpent.load(event.data)
	
	if not ok then
		log("Hero Game Co. discovery server couldn't deserialize a request.\nevent.data: '" .. tostring(event.data) .. "'")
		return
	end
	
	local action = DiscoveryServer[string.upper(request.action)]
	if action then
		if request.uuid then
			log('executing request action ' .. tostring(request.action) .. ' for user ' .. tostring(request.uuid))
		else
			log('executing request action ' .. tostring(request.action) .. ' for anonymous user')
		end
		request.action = nil
		action(request, event)
	end
end
local function acknowledge(event)
	event.peer:send(serpent.dump({success = true}))
end
local function reject(event, rejection)
	log(rejection .. "\nevent: " .. serpent.block(event))
	event.peer:send(serpent.dump({success = false}))
end
local function onConnect(event)
	if verbose_logging then
		log('connected to ' .. tostring(event.peer))
	end
	event.peer:timeout(5, 1000, 5000)
	log('inserting to peers[' .. event.peer:connect_id() .. ']')
	peers[event.peer:connect_id()] = {peer = event.peer}
end
local function onDisconnect(event)
	if verbose_logging then
		log('disconnected from peer\nevent: ' .. serpent.block(event))
	end
end
local function setLobby(lobby, event, key)
	-- handle common tasks of hiding password and setting owner address
	local stored_peer = peers[event.peer:connect_id()]
	if not stored_peer then
		log("can't find stored_peer for this event:\n" .. serpent.block(event))
		return
	end
	
	stored_peer.lobby_id = key
	if lobby.password then
		lobby.password = true
	end
	local owner_ip_address = tostring(event.peer):match("(.+):")
	lobby.owner = owner_ip_address
	lobbies[key] = lobby
end

function DiscoveryServer.CREATE_LOBBY(request, event)
	-- grab uuid from user request and then remove it
	local user_uuid = request.uuid
	request.uuid = nil
	lastpeer = event.peer
	
	-- validate request
	if not user_uuid then
		reject(event, "can't create a lobby without a uuid present in the request")
		return
	end
	if authentication[user_uuid] then
		reject(event, "there's already a lobby associated with this uuid")
		return
	end
	if not game_list[string.upper(request.game)] then
		reject(event, "can't create lobby for game with name '" .. tostring(request.game) .. "'")
		return
	end
	if not request.name then
		reject(event, "can't create a lobby with no name")
		return
	end
	
	-- generate public uuid to associate with user's private uuid
	local lobby_id = string.uuid()
	authentication[user_uuid] = lobby_id
	
	-- remember uuid for this connected user, so when they disconnect, we can close lobby
	local stored_peer = peers[event.peer:connect_id()]
	if not stored_peer then
		log("can't find stored_peer for this event:\n" .. serpent.block(event))
		return
	end
	stored_peer.user_uuid = user_uuid
	stored_peer.lobby_id = lobby_id
	
	setLobby(request, event, lobby_id)
	
	acknowledge(event)
end
function DiscoveryServer.UPDATE_LOBBY(request, event)
	-- grab uuid from user request and then remove it
	local user_uuid = request.uuid
	request.uuid = nil
	
	-- validate request
	if not user_uuid then
		reject(event, "can't update a lobby without a uuid present in the request")
		return
	end
	local lobby_id = authentication[user_uuid]
	if not lobby_id then
		reject(event, "couldn't find a lobby_id associated with this request's uuid " .. tostring(user_uuid))
		return
	end
	local lobby = lobbies[lobby_id]
	if not lobby then
		reject(event, "couldn't find the lobby associated with this lobby_id" .. tostring(lobby_id))
		return
	end
	
	-- this will overwrite lobbies[lobby_id] using request lobby data
	setLobby(request, event, lobby_id)
	acknowledge(event)
end
function DiscoveryServer.CLOSE_LOBBY(request, event)
	-- grab uuid from user request and then remove it
	local user_uuid = request.uuid
	request.uuid = nil
	
	-- validation
	if not user_uuid then
		reject(event, "can't close a lobby without a uuid present in the request")
		return
	end
	local lobby_id = authentication[user_uuid]
	if not lobby_id then
		reject(event, "couldn't find a lobby_id associated with this request's uuid " .. tostring(user_uuid))
		return
	end
	local lobby = lobbies[lobby_id]
	if not lobby then
		reject(event, "couldn't find the lobby associated with this lobby_id" .. tostring(lobby_id))
		return
	end
	
	lobbies[lobby_id] = nil
	local stored_peer = peers[event.peer:connect_id()]
	if not stored_peer then
		log("can't find stored_peer for this event:\n" .. serpent.block(event))
		return
	end
	stored_peer.lobby_id = nil
	authentication[stored_peer.user_uuid] = nil
	acknowledge(event)
end
function DiscoveryServer.GET_LOBBIES(request, event)
	event.peer:send(serpent.dump({
		lobbies = lobbies,
		success = true
	}))
end

function DiscoveryServer.restart()
	log('Restarting the Discovery Server')
	lobbies, authentication = {}, {}
	host = enet.host_create(server_address or "localhost:6789", max_peers, channel_count, bandwidth, bandwidth)
end
function DiscoveryServer.service()
	local event = host:service(100)
	if event then
		if event.type == 'connect' then
			onConnect(event)
		end
		if event.type == 'receive' then
			parseRequest(event)
		end
		if event.type == 'disconnect' then
			onDisconnect(event)
		end
	end
	event = host:service()
end

-- integration

function love.update(dt)
	DiscoveryServer.service(100)
	if love.timer.getTime() > last_peer_check + 1 then
		checkPeers()
	end
end

-- function love.keypressed(key)
	-- if key == 'l' then
		-- log('lobbies:\n' .. serpent.block(lobbies))
	-- end
	
	-- if key == 'p' then
		-- log('peers:\n' .. serpent.block(peers))
	-- end
	
	-- if key == 'a' then
		-- log('authentication:\n' .. serpent.block(authentication))
	-- end
	
	-- if key == 'f10' then
		-- debug.debug()
	-- end
-- end