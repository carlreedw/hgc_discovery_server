--[[
PROJECT: LOBBYSERVER
AUTHORS: carl
DATE: 2021-04-17
DESC: initially, provide player lobby service to mastersqueak players
RUNH: "C:/root/love2d/11.3/love.exe" "C:/root/dev/hgc_discovery_server"
--]]

enet = require "enet"
serpent = require "serpent/serpent"
-- server_address = "www.herogameco.com:6789"
server_address = "206.81.6.118:6789"
-- server_address = "localhost:6789"
string.uuid = function()
	-- from rxi lume I think [[https://github.com/rxi/lume]]
	local fn = function(x)
		local r = love.math.random(16) - 1
		r = (x == "x") and (r + 1) or (r % 4) + 9
		return ("0123456789abcdef"):sub(r, r)
	end
	return (("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx"):gsub("[xy]", fn))
end

function love.load(filtered_args, unfiltered_args)
	for i, arg in ipairs(unfiltered_args) do
		if arg == '--test' then
			require 'love.window'
			require 'love.graphics'
			love.window.setMode(800, 600)
			require "test"
			return
		end
	end
	
	require "discovery"
end