--[[
should be run with love2d as it requires enet
RUN: "C:/root/love2d/11.3/love.exe" "C:/root/dev/lobbyserver/tests"

press keypad 1-6 to run tests 1-6
press p at any time to print test results
]]

print('STARTING TEST SEQUENCE')
client = enet.host_create('*:0')
-- client = enet.host_create()
print('CONNECTED "client" to server: ' .. tostring(server))
timeout_length = 100
events = {}
uuid = string.uuid()

function love.update(dt)
	local event = client:service(timeout_length)
	if event then
		print('event occured: ' .. serpent.block(event))
		table.insert(events, event)
		if onEvent then
			onEvent(event)
		end
	end
end

function love.keypressed(key)
	for i, test in ipairs(tests) do
		if key == 'kp' .. i then
			print('running test ' .. i .. ' -- ' .. test.desc)
			test.run()
		end
	end
	
	if key == 'p' then
		printresults()
	end
	
	if key == 'd' and server then
		server:disconnect()
		function onEvent()
			server = nil
		end
	end
	
	if key == 'f10' then
		debug.debug()
	end
end

tests = {}	-- have desc, pass, error
tests[1] = {
	desc = "Enet clients can connect to the discovery server",
	run = function()
		local thistest = tests[1]
		thistest.pass = false
		if server then
			thistest.error = "already connected to server"
			return
		end
		function onEvent(event)
			if event.type == 'connect' then
				thistest.pass = true
				print('test 1 passed')
			else
				thistest.error = "event from client:service not of type 'connect' -- has type " .. tostring(event.type)
			end
		end
		
		-- try to connect
		server = client:connect(server_address)
	end
}
tests[2] = {
	desc = "Enet clients can create lobbies",
	run = function()
		local thistest = tests[2]
		thistest.pass = false
		if not server then
			thistest.error = "must be connected to discovery server to test"
			return
		end
		function onEvent(event)
			if event.type == 'receive' then
				local ok, response = serpent.load(event.data)
				if ok then
					if response.success then
						print('test 2 passed')
						thistest.pass = true
					end
				else
					thistest.error = "couldn't deserialize event data into response table"
				end
			else
				thistest.error = "event from client:service not of type 'receive' -- event: " .. serpent.block(event)
			end
		end
		
		-- test sending CREATE_LOBBY request
		msg = {
			action = "CREATE_LOBBY",
			game = "MASTERSQUEAK",
			player_slots = 3,
			username = "TWOHITS",
			name = "KIWI SECRET GARDEN",
			password = "boogaboomonster",
			uuid = uuid
		}
		server:send(serpent.dump(msg))
	end
}
tests[3] = {
	desc = "Enet clients can query the server for lobby information",
	run = function()
		local thistest = tests[3]
		thistest.pass = false
		if not server then
			thistest.error = "must be connected to discovery server to test"
			return
		end
		function onEvent(event)
			if event.type == 'receive' then
				local ok, response = serpent.load(event.data)
				if ok then
					if response.success and response.lobbies then
						print('test 3 passed')
						thistest.pass = true
					else
						thistest.error = "got response table, but either not success or no lobbies property"
					end
				else
					thistest.error = "couldn't deserialize event data into response table"
				end
			else
				thistest.error = "event from client:service not of type 'receive' -- event: " .. serpent.block(event)
			end
		end
		msg = {
			action = "GET_LOBBIES"
		}
		server:send(serpent.dump(msg))
		
	end
}
tests[4] = {
	desc = "Enet clients can update lobby data for lobbies they've created",
	run = function()
		local thistest = tests[4]
		thistest.pass = false
		function onEvent(event)
			if event.type == 'receive' then
				local ok, response = serpent.load(event.data)
				if ok then
					if response.success then
						print('test 4 passed')
						thistest.pass = true
					else
						thistest.error = "got response table, but no success"
					end
				else
					thistest.error = "couldn't deserialize event data into response table"
				end
			else
				thistest.error = "event from client:service not of type 'receive' -- event: " .. serpent.block(event)
			end
		end
		
		-- test updating lobby info
		msg = {
			action = "UPDATE_LOBBY",
			name = "KIWI SUPER SECRET GARDEN",
			uuid = uuid
		}
		server:send(serpent.dump(msg))
	end
}
tests[5] = {
	desc = "Enet clients can't update lobby data for lobbies created by other users",
	run = function()
		local thistest = tests[5]
		thistest.pass = false
		function onEvent(event)
			if event.type == 'receive' then
				local ok, response = serpent.load(event.data)
				if ok then
					if response.success == false then
						print('test 5 passed')
						thistest.pass = true
					else
						thistest.error = "got response table, but update was succesful! or missing error message"
					end
				else
					thistest.error = "couldn't deserialize event data into response table"
				end
			else
				thistest.error = "event from client:service not of type 'receive' -- event: " .. serpent.block(event)
			end
		end
		
		-- test updating lobby info
		msg = {
			action = "UPDATE_LOBBY",
			game = "MASTERSQUEAK",
			name = "KIWI SECRET GARDEN",
			new_property = "new data2",
			uuid = string.uuid()
		}
		server:send(serpent.dump(msg))
	end
}
tests[6] = {
	desc = "Enet clients can close lobbies they've previously created",
	run = function()
		local thistest = tests[6]
		thistest.pass = false
		function onEvent(event)
			if event.type == 'receive' then
				local ok, response = serpent.load(event.data)
				if ok then
					if response.success then
						print('test 6 passed')
						thistest.pass = true
					else
						thistest.error = "got response table, but no success"
					end
				else
					thistest.error = "couldn't deserialize event data into response table"
				end
			else
				thistest.error = "event from client:service not of type 'receive' -- event: " .. serpent.block(event)
			end
		end
		
		-- test updating lobby info
		msg = {
			action = "CLOSE_LOBBY",
			uuid = uuid
		}
		server:send(serpent.dump(msg))
	end
}

function printresults()
	print ("TEST RESULTS:")
	for i, test in ipairs(tests) do
		print(string.format("TEST %s.) PASS: %s -- DESC: %s -- ERROR: %s", i, test.pass, test.desc, test.error))
	end
	print("\nTESTING COMPLETE")
end
